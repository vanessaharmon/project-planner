# Projectplanner

- This project was developed and maintained by [Your Name]. It is a solo project, meaning that I was the sole contributor and made all the development decisions.


Full Stack web application for organizing and tracking projects and tasks.

## Functionality

- My Projects: Add new projects projects list.
- My Tasks: Add tasks to projects.

## User Stories/Scenarios :cucumber:

- `Given` a user is logged in, `When` they add a project to their projects list, `Then` the project is added to the projects list
- `Given` a user is logged in, `When` they add a task to a project, `Then` the task will show under the projects details or 'My Tasks' page
- `Given` a user is logged in, `When` they search a key word, `Then` a list of projects with that key word will populate

## Intended Market

Aimed at individuals looking to manage their project and tasks

## Stretch goals

- Enabling the calendar to update with tasks due dates 
- Having a list of upcoming tasks 
- The abilty to connect with other users working on a project with you 

## Onboarding

- To use the application the way it was intended make sure to follow the steps below.

1. Fork and clone the starter project from Project Alpha 
2. Create a new virtual environment in the repository directory for the project
3. Activate the virtual environment
4. Upgrade pip
5. Install django
6. Install black
7. Install flake8
8. Install djlint
9. Deactivate your virtual environment
10. Activate your virtual environment
11. Use pip freeze to generate a requirements.txt file

## Tech Stack :books:

- HTML5
- django
- CSS
- Bootstrap

